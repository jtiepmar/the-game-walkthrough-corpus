# Game Walkthrough Corpus

Video games are one of the most influential entertainment mediums in our world and influence large parts of the society directly as a means of entertainment, education and recreation or indirectly as it is seen in social gamification processes. A multi billion dollar market has developed around this medium that is in some cases even under legal observation because of harmful effects on parts of the population.

Yet the video game world is not yet a prominent part of the scientific research process. One reason is that it is hard to answer the question how an analysis should be handled. Video games consist of bytecode that can not really be interpreted constructively and are mainly consumed via audiovisual means, which are hard enough to analyse for themselves. Another reason is also one of video games' defining properties: Interactivity. How would we be able to analyse something when its actual content is in big parts created and controlled by the person that consumes it?

The purpose of this project is to provide a solution for both of these problems and hopefully a robust starting point for empirical work in the field of Games Studies. Video game walkthroughs provide a textual representation of the video game in question and contain exactly the information that is needed to complete the game. These descriptions ignore the (theoretically infinite) variance of outcomes that are the result of the interaction element. Additionally they convert the content of a video game into text, an information medium that is routinely analysed in many ways in various research environments. 

## Citation

Paper: Burghardt, M. and Tiepmar, J., 2021. The Game Walkthrough Corpus (GWTC) - A Resource for the Analysis of Textual Game Descriptions. Journal of Open Humanities Data, 7, p.14. DOI: http://doi.org/10.5334/johd.34 

Dataset: Tiepmar, J., and Burghardt, M., 2021. Game Walkthrough Corpus (GWTC) (Version 1.0) [Data set]. Zenodo. http://doi.org/10.5281/zenodo.4562336

## Copyright Information 

Game walkthroughs are protected by individual copyright notices that are often very strict. 
That is why this data set does not include the documents but instead various data formats that are useful for text mining and distant reading methods while not allowing to recreate the documents. It is highly unlikely that even a single sentence can be reconstructed from the published data. 

Since the documents are not -- not even in part -- published but only text mining statistics about them, no violation of copyright is done by this project. 
The data that is made available here is published as Creative Commons CC BY 3.0.

Links to the original documents are available in the data section.


## Project Overview

* Goal: The goal of this project is to publish a text corpus that compiles video game walkthroughs from various sources for textual analysis. 
* Project Coordinator: Dr. Jochen Tiepmar, Natural Language Processing Group, Leipzig University  
* Project Start: 12.02.2020  
* Project End: "When it's done"  
* Contact: jtiepmar(at)informatik.uni-leipzig.de

## Project Statistics

* Walkthrough Sources:	portforward neoseeker spieletipps jayisgames gamesetter
* Number of Games:	6013
* Game Language Associations: 	4631
* Genre Associations: 	3806
* Gameplay Tags: 	10246
* Release Dates: 	2443
* Developers: 	3152
* Publishers: 	2782
* Steam IDs: 	1086
* Platform Associations: 	5293 (PC, Gameboy, iOS, Linux,...)

## Text Corpus Statistics
Documents: 12295

|statistic per Document     |min |max	  |sum	 |avg	 |stddev |
| ------------- | -- |------  |------------- |------------- |------------- |
 |typecount	    |6	 |593'807  | 149'932'893 |	12'194.62 |	22'953.46 |
 |textlength	|38  |4'774'829 | 944'775'818 |	76'842.28 |	155'231.11 |
 |tokencount	|1	 |16'761   |	16'122'040 |	1'311.27 |	1'217.55 |
 |typetokenratio|1	 |415     |	84'481	 |6.87	 |6.5 |
 |typetokenratio|1	 |415     |	84'481	 |6.87	 |6.5 |
|game_walkthrough_stats|	1|	181|12'295	|2.04|	4.59|


## Data

The data creation is documented via this sourcecode: https://bitbucket.org/jtiepmar/game-walkthrough-corpus/src/master/doc/createdata.py that uses a newline seperated list of texts as input (each line identifier [TAB] text).

Currently the data includes (each per document):

* URLs to the original text plus release dates
* Type/Token statistics
* Bigrams (Higher nGrams can not be published because of copyright restrictions)
* Bag of Words
* Randomized sentence collocations
* Game-Walkthrough Mapping

If you are interested in specific data formats that are not covered, feel free to contact me. To comply with copyright regulations, all data are randomized and provided in a way that makes it impossible to recreate the documents (or even a single sentence) while still being useful for analysis. 

More data is added as the project continues.

Bitbucket: https://bitbucket.org/jtiepmar/game-walkthrough-corpus/src/master/data/

The latest update and some diagrams are available here : http://www.informatik.uni-leipzig.de/~jtiepmar/forschung/gwtc/

## Metadata

The metadata is compiled from Steam and RAWG, which means there is a serious PC-Bias but console games are also included. 

Currently the metadata includes (each per document): 

* Developer and Publisher names
* Genre and Gameplay tags
* Supported languages
* Release dates
* Platforms (PC, Game Boy, IOS,...)
* Short (publisher) descriptions of the games

More metadata is added as the project continues.

Bitbucket: https://bitbucket.org/jtiepmar/game-walkthrough-corpus/src/master/metadata/

The latest update and some diagrams are available here : http://www.informatik.uni-leipzig.de/~jtiepmar/forschung/gwtc/

## (Optional) Roadmap

(Suggestions, hints and help are welcome)

* Links to Diggr and Wikidata
* Age Ratings (worldwide)
* More Languages
* Metadata based Subcorpus Creation
* CTS and online Text Mining Tools
* Monetization Information
* More Walkthroughs
* Game Reviews
* More Metadata Linkage
