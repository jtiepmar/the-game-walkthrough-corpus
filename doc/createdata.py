import os
import re
import random
import math
import shutil

if not os.path.exists("data"):
	os.mkdir("data")
if not os.path.exists("metadata"):
	os.mkdir("metadata")
if not os.path.exists("datazip"):
	os.mkdir("datazip")

def typetokenratio(text):
	dc = {}
	text=text.lower()
	text = re.sub(r'[^\w\s]',' ',text)

	whitelist = set('abcdefghijklmnopqrstuvwxyzäöüß ')
	text = ''.join(filter(whitelist.__contains__, text))
	while("  " in text):
		text = text.replace("  "," ")
	arr = text.split(" ")
	for type in arr:
		if len(type)>0 and not type in dc:
			dc[type] = 1

	if len(dc.keys()) > 0:
		return len(arr)/len(dc.keys())
	else:
		return 0
	
def typecount(text):
	text=text.lower()
	text = re.sub(r'[^\w\s]',' ',text)

	whitelist = set('abcdefghijklmnopqrstuvwxyzäöüß ')
	text = ''.join(filter(whitelist.__contains__, text))
	while("  " in text):
		text = text.replace("  "," ")
	text = text.strip()
	if len(text)>0:
		types = len(text.split(" "))+1
	else:
		types=0
	return types

def tokencount(text):
	dc = {}
	text=text.lower()
	text = re.sub(r'[^\w\s]',' ',text)

	whitelist = set('abcdefghijklmnopqrstuvwxyzäöüß ')
	text = ''.join(filter(whitelist.__contains__, text))
	while("  " in text):
		text = text.replace("  "," ")
	arr = text.split(" ")
	for type in arr:
		if len(type)>0 and not type in dc:
			dc[type] = 1
	return len(dc.keys())

def sentencecollocations(text):
	sentencelist=[]
	text=text.lower()
	text = re.sub(r'[(*\\!)*?]','.',text)
	sentences= text.split(".")
	random.shuffle(sentences)
	for sent in sentences:
		dc = {}
		whitelist = set('abcdefghijklmnopqrstuvwxyzäöüß. ')
		sent = ''.join(filter(whitelist.__contains__, sent))
		while("  " in sent):
			sent = sent.replace("  "," ")
		arr = sent.split(" ")
		for type in arr:
			if len(type)>0:
				if type in dc:
					dc[type] = dc[type]+1
				else:
					dc[type] = 1
		l = list(dc.items())
		random.shuffle(l)
		sentencelist.append(dict(l))
		
	return sentencelist
	
def bagofwords(text):
	dc = {}
	text=text.lower()
	text = re.sub(r'[^\w\s]',' ',text)

	whitelist = set('abcdefghijklmnopqrstuvwxyzäöüß ')
	text = ''.join(filter(whitelist.__contains__, text))
	while("  " in text):
		text = text.replace("  "," ")
	arr = text.split(" ")
	for type in arr:
		if len(type)>0:
			if type in dc:
				dc[type] = dc[type]+1
			else:
				dc[type] = 1
	l = list(dc.items())
	l.sort(key=lambda x: x[1], reverse=True)
	return dict(l)
	
def ngrams(text,size):
	dc = {}
	text=text.lower()
	text = re.sub(r'[^\w\s]',' ',text)

	whitelist = set('abcdefghijklmnopqrstuvwxyzäöüß ')
	text = ''.join(filter(whitelist.__contains__, text))
	while("  " in text):
		text = text.replace("  "," ")
	arr = text.split(" ")
	i = 1
	while(i<len(arr)):
		ngram = ""
		n = 0
		while(n<=size and i+n<len(arr)):
			ngram = ngram+" "+arr[i+n]
			n=n+1
			if n==size:
				ngram=ngram.strip()
				if ngram in dc:
					dc[ngram] = dc[ngram]+1
				else:
					dc[ngram] = 1
		i = i+1
	l = list(dc.items())
	l.sort(key=lambda x: x[1], reverse=True)
	return dict(l)

def urnstats():
	oldid=""
	gwmapping = open("data/game_walkthrough_mapping.txt", "w", encoding='utf8')
	first = True
	firstUrn = True
	with open("passages.txt", encoding='utf8') as f:
		for line in f:
			name_urn = line.split("\t")
			game = name_urn[0].split(".walkthrough.")[0]+":"
			if oldid != game:
				if not first:
					gwmapping.write("\n")
				gwmapping.write(game.strip())
				first=False
				firstUrn = True
				oldid=game
			if not firstUrn:
				gwmapping.write(","+name_urn[0].strip())
			if firstUrn:
				gwmapping.write("\t"+name_urn[0].strip())
				firstUrn = False
	gwstats = open("data/game_walkthrough_stats.txt", "w", encoding='utf8')
	gwmapping.close()
	with open("data/game_walkthrough_mapping.txt", encoding='utf8') as f:
		for line in f:
			game_docs = line.split("\t")
			doccount=len(game_docs[1].split(","))
			gwstats.write(game_docs[0]+"\t"+str(doccount)+"\n")
	gwstats.close()
	
def tfidf(filter):
	if  not os.path.exists("data/bagofwords.txt"):
		return ""
	tokenlist={}
	doccount=0
	with open ("data/bagofwords.txt") as f:
		for line in f:
			urn = line.split("\t")[0]
			if not filter in urn:
				continue
			line = line.split("\t")[1]
			doccount = doccount+1
			line = line.replace("{","").replace("}","")
			line = re.sub(r'[0-9]+', '', line)
			arr = line.replace("'","").replace(":","").split(",")
			for token in arr:
				token = token.strip()
				if token in tokenlist.keys():
					tokenlist[token] = tokenlist[token] + 1
				else:	
					tokenlist[token] = 1
			print(str(doccount))

	out = open("data/docfrequency"+filter.replace(".","_").replace(":","_")+".txt", "w", encoding='utf8')
	for token in tokenlist:
		idf = math.log(doccount//tokenlist[token])+1
		out.write(token+"\t"+str(tokenlist[token])+"\t"+str(idf)+"\n")
		tokenlist[token] = idf
	out.flush()
	out.close()

	out = open("data/tfidf"+filter.replace(".","_").replace(":","_")+".txt", "w", encoding='utf8')
	with open ("data/bagofwords.txt") as f:
		for line in f:
			urn = line.split("\t")[0]
			if not filter in urn:
				continue
			line = line.replace("{","").replace("}","")
			line = line.split("\t")[1]
			documenttoken = {}
			arr = line.replace("'","").split(",")
			for token in arr:
				token_count=token.split(":")
				tok = token_count[0].strip()
				tf = int(token_count[1].strip())
				documenttoken[tok]=round(tf*tokenlist[tok],2)
			l = list(documenttoken.items())
			l.sort(key=lambda x: x[1], reverse=True)
			out.write(urn+"\t"+str(dict(l))+"\n")
	out.flush()
	out.close()
	return 
				
def measure(function):


	with open("passages.txt", encoding='utf8') as f:
		out = open("data/"+function+".txt", "w", encoding='utf8')

		for line in f:
			urn_text=line.split("\t")
			text=urn_text[1]
			result = ""
			if function == "textlength": 
				result=len(text.strip())
			if function == "tokencount": 
				result=tokencount(text)
			if function == "typecount": 
				result=typecount(text)
			if function == "typetokenratio": 
				result=typetokenratio(text)
			if function == "bagofwords": 
				result=bagofwords(text)
			if function == "trigrams": 
				result=ngrams(text,3)
			if function == "pentograms": 
				result=ngrams(text,5)
			if function == "bigrams": 
				result=ngrams(text,2)
			if function == "sentencecollocations": 
				result=sentencecollocations(text)

			out.write(urn_text[0]+"\t"+str(result)+"\n")
			print(urn_text[0])
		out.close()
		
def stats(function):
	min = 1000000
	max = 0
	n=0
	sum=0
	dev = 0
	with open("data/"+function+".txt", encoding='utf8') as f:
		for line in f:
			val = float(line.split("\t")[1])	
			if max<val:
				max=val
			if min>val:
				min=val
			sum=sum+val
			
			n=n+1
		avg=sum/n
	with open("data/"+function+".txt", encoding='utf8') as f:
		for line in f:
			val = float(line.split("\t")[1])	
			dev = dev + (val-avg)*(val-avg)
	dev = math.sqrt(dev/n)

	if not os.path.exists("data/corpusstats.txt"):
		corpusstats = open("data/corpusstats.txt", "w", encoding='utf8')
		corpusstats.write("statistic\tmin\tmax\tsum\tavg\tstddev\n")
	else:
		corpusstats = open("data/corpusstats.txt", "a", encoding='utf8')
	corpusstats.write(function+"\t"+str(int(min))+"\t"+str(int(max))+"\t"+str(int(sum))+"\t"+str(round(avg,2))+"\t"+str(round(dev,2))+"\n")
	corpusstats.close()




#measure("trigrams")
#measure("pentograms")
#measure("textlength")
#measure("tokencount")
#measure("typecount")
#measure("typetokenratio")
#measure("bigrams")
#measure("bagofwords")
#measure("sentencecollocations")
#urnstats()
tfidf(".deu.")
tfidf(".eng.")
#stats("typecount")
#stats("textlength")
#stats("tokencount")
#stats("typetokenratio")
#stats("game_walkthrough_stats")

shutil.copyfile("createdata.py", "repo/game-walkthrough-corpus/doc/createdata.py")


