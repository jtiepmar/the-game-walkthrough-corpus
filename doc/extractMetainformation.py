import os
import pandas as pd
import datetime
import shutil



	 

data = {}
found = 0

def cleanLanguages(str):
	str = str.replace("b/b", "")
	str = str.replace("supported_languages", "")
	str = str.replace("(text only)", "")
	str = str.replace("with full audio support", "")
	str = str.replace("languages", "")
	return str.strip()
def normalizedate(str):
	if "Coming this" in str:
		return ""
	if "Coming" in str:
		return ""
		
	str = str.replace("Jan,", "01").replace("Feb,", "02").replace("Mar,", "03").replace("Apr,", "04").replace("May,", "05").replace("Jun,", "06").replace("Jul,", "07").replace("Aug,", "08").replace("Sep,", "09").replace("Oct,", "10").replace("Nov,", "11").replace("Dec,", "12")
	str = str.replace("Jan", "01").replace("Feb", "02").replace("Mar", "03").replace("Apr", "04").replace("May", "05").replace("Jun", "06").replace("Jul", "07").replace("Aug", "08").replace("Sep", "09").replace("Oct", "10").replace("Nov", "11").replace("Dec", "12")
	str = str.replace(" ",".")
	while(".." in str):
		str = str.replace("..",".")
	while(",." in str):
		str = str.replace(",.",",")
	dates = str.split(",")
	str = ""
	res = []
#	print(str)
	for date in dates:

		if(len(date)>4):
			dmy = date.split(".")
			if(len(dmy)==2):
				date = "01."+date
				dmy = date.split(".")
			if(len(dmy[0])==1):
				dmy[0] = "0"+dmy[0]
			if(len(dmy[1])==1):
				dmy[1] = "0"+dmy[1]

			if(len(dmy[2])==2 and int(dmy[2])<70):
				dmy[2] = "20"+dmy[2]
			if(len(dmy[2])==2 and int(dmy[2])>=70):
				dmy[2] = "19"+dmy[2]
			date = dmy[2]+"-"+dmy[1]+"-"+dmy[0]
			now = datetime.datetime.now()
#			+ " " +now.year<int(date[2]))
			if int(dmy[2]) > int(now.year):
				return ""
			res.append(date)
	return ",".join(res)

def extractFromFile(filename, lookfor):
	count = 0
	hit=-1
	global data
	with open(filename, encoding='utf8') as f:
		legend = f.readline()
		legenarr = legend.split("\t")
		for i in legenarr:
			if i.strip() == lookfor:
				hit=count 
			count = count+1
			
		if hit >0:
			for line in f:
				line = line.replace('"', "").replace('\n', "")
				line = line.replace("<strong>*</strong>","").replace("*</strong>languages with full audio support", "").replace("*", "")
				#line= line.replace('Артогон', "Artogon").replace('languages with full audio support', "").replace('Montréal', "Montreal").replace('Zoë', "Zoe").replace('Zoё', "Zoe").replace('МиСТ ленд-ЮГ', "Mist Lendjut").replace('Сатурн-плюс', "Saturn Plus").replace('1С: Ino-Co', "1C: Inoco").replace('Акелла', "Akella")
				line = line.replace('<ul class=bb_ul>', " ").replace('<hr />', " ").replace('<ol>', " ").replace('</ol>', " ").replace('<h1>', " ").replace('</h1>', " ").replace('<h2>', " ").replace('</h2>', " ").replace('<h3>', " ").replace('</h3>', " ").replace('<code>', " ").replace('</code>', " ").replace('<pre>', " ").replace('</pre>', " ").replace('<br />', " ").replace('</li>', " ").replace('<li>', " ").replace('</ul>', " ").replace('<ul>', " ").replace('</strong>', " ").replace('<strong>', " ").replace('<strong>*</strong>', " ").replace('</em>', " ").replace('<em>', " ").replace('<br>', " ").replace('[b]*[/b]', " ").replace('<p>', " ").replace('</p>', " ").replace('<br/>', " ")
				#line = line.replace("α", "alpha").replace("Ω", "Omega").replace("❤", "_heart_").replace("👻", "_ghost_").replace("δ", "delta").replace("Δ", "Delta").replace("▸", "").replace("►", " ").replace("ā", "a").replace("ō", "o").replace("・", " ").replace("ū", "u").replace("ī", "i").replace("♥", "_heart_")
				while("  " in line):
					line = line.replace("  "," ")
				arr = line.split("\t") 
				if len(arr) == count:
					urn = arr[0].split(".")[0]+":"
					urn=urn.replace("urn:cts:gsc:","urn:cts:gwtc:")
					if urn in data.keys():
						data[urn] = data[urn]+", "+arr[hit]
					else:
						data[urn] = arr[hit]
						
			#else:
				#print(line)

def removeambiguity(string):
	return string.replace("windows", "Windows").replace("linux", "Linux").replace("linux", "Linux").replace("macOS", "mac").replace("mac", "Mac").replace("Single-player","Singleplayer").replace("Multi-player","Multiplayer").replace("Co-op","Coop").replace("Spanish - Spain", "Spanish").replace("English*", "English")
	
def removeDuplicates(string):
	string = removeambiguity(string).strip()
	arrDupl = string.split(",")
	tmp = list()
	for entry in arrDupl:
		entry = entry.strip()
		if entry not in tmp and len(entry)>0:
			tmp.append(entry)
	uniqueString = ",".join(tmp)
	return uniqueString
	
def removeGarbage(string):
	string = string.replace(",Inc.", "")
	
	return string
	

def extract(info):
	global found
	found=0
	global data
	data = {}


	if os.path.isfile("metadata/"+info+".txt"):
		os.remove("metadata/"+info+".txt")

	extractFromFile('metadata_steam.csv', info)
	extractFromFile('metadata_rawg.csv', info)
	f = open("metadata/"+info+".txt", "a",encoding="utf-8")
	for key in sorted(data.keys()):
		values = data[key].strip()
		values = values.replace("'","").replace("[","").replace("]","").replace(",,",",")
		if(info=="release_date"):
			values = normalizedate(values)
		if(info=="supported_languages"):
			values = cleanLanguages(values)
		if not info == "description":
			values = removeDuplicates(values)
			values = removeGarbage(values)
		if len(values)>1:
			#if key == "urn:cts:gsc:midnight_mysteries_the_edgar_allan_poe_conspiracy:":
			#	print(values)


			arr = values.split(",")
			found = found + len(arr)
			f.write(key+"\t" +values+"\n")
	f.close()
	print(info+": "+str(found)+" extracted")

def mergeData():
	if os.path.isfile("metadata/_all.txt"):
		os.remove("metadata/_all.txt")

	header = "urn"
	dir_tree = os.walk("metadata")
	for dirpath, dirnames, filenames in dir_tree:
		pass
		

	data = {}
	csv_list = []
	variables=[]
	for file in filenames:
		if file.endswith('.txt') and not file.startswith("_mapping"):
			header = header +"\t"+file.replace(".txt", "")
			variables.append(file.replace(".txt", ""))
			with open("metadata/"+file, encoding='utf8') as f:
				for line in f:
					urn_meta = line.split("\t")
					urn = urn_meta[0].strip()
					if(len(urn)<5):
						print(file)
					meta = urn_meta[1].strip()
					values = {}
					if urn in data.keys():
						values = data[urn]
					values[file.replace(".txt", "")] = meta
					data[urn] = values
	f = open("metadata/_all.txt", "w", encoding='utf8')
	f.write(header+"\n")
	for urn in sorted(data.keys()):
		line = urn
		for var in variables:
			dc = data[urn]
			val = "\t"
			if var in dc.keys():
				val = val + dc[var]
			line = line + val
		line = line + "\n"
		f.write(line)
	f.close()

if not os.path.isdir("metadata"):
	os.mkdir("metadata")

def extractCTSData():
	oldid=""
	urns = open("metadata/urns.txt", "w", encoding='utf8')
	titles = open("metadata/titles.txt", "w", encoding='utf8')
	first = True
	firstUrn = True
	with open("titlesandurns.txt", encoding='utf8') as f:
		for line in f:
			line=line.replace("urn:cts:gsc:", "urn:cts:gwtc:")
			name_urn = line.split("\t")
			id = name_urn[1].split(".walkthrough.")[0]+":"
			if oldid != id:
				titles.write(id.strip()+"\t"+name_urn[0].strip()+"\n")
				if not first:
					urns.write("\n")
				urns.write(id.strip())
				first=False
				firstUrn = True
			oldid=id
			if not firstUrn:
				urns.write(","+name_urn[1].strip())
			if firstUrn:
				urns.write("\t"+name_urn[1].strip())
				firstUrn = False

	urns.close()
	titles.close()

def map(key, value):
	return ""

def map(key, value):
	if os.path.isfile("metadata/"+key+"_"+value+".txt"):
		os.remove("metadata/"+key+"_"+value+".txt")		

	urn_values = {}
	with open("metadata/"+value+".txt", encoding='utf8') as f:
		for line in f:
			tmp = line.split("\t")
			urn_values[tmp[0]] = tmp[1].strip()
	urn_keys = {}
	with open("metadata/"+key+".txt", encoding='utf8') as f:
		for line in f:
			tmp = line.split("\t")
			urn_keys[tmp[0]] = tmp[1].strip()
	f = open("metadata/_mapping_"+key+"_"+value+".txt", "w", encoding='utf8')

	for valurn in urn_values.keys():
		if valurn in urn_keys.keys():
			tmp = urn_keys[valurn].split(",")
			for entry in tmp:
				f.write(urn_values[valurn]+"\t"+entry+"\t"+valurn+"\n")
	f.close()

				

	
	
extractCTSData()
extract("genres")
extract("tags")
extract("supported_languages")
extract("platforms")
extract("description")
extract("publishers")
extract("developers")
extract("release_date")
mergeData()
map("release_date", "genres")
map("release_date", "tags")
map("release_date", "supported_languages")
map("release_date", "platforms")
map("release_date", "publishers")
map("release_date", "developers")

shutil.copyfile("extractMetainformation.py", "repo/game-walkthrough-corpus/doc/extractMetainformation.py")
shutil.copyfile("metadata_rawg.csv", "repo/game-walkthrough-corpus/doc/metadata_rawg.csv")
shutil.copyfile("metadata_steam.csv", "repo/game-walkthrough-corpus/doc/metadata_steam.csv")
shutil.copyfile("metadata_symbol.csv", "repo/game-walkthrough-corpus/doc/metadata_symbol.csv")
shutil.copyfile("titlesandurns.txt", "repo/game-walkthrough-corpus/doc/titlesandurns.txt")

